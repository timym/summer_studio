% Before running this script, please read the Matlab section of the dobot_magician wiki at:
% https://bitbucket.org/jamestkpoon/dobot_magician/wiki/Home

%%
            
rosinit('http://localhost:11311') %connect to ros network change localhost to ip address of roscore if needed

%% subscribe to Magician state
sub = rossubscriber('/dobot_magician/state');
pause(1);
%receive and print latest state message
disp 'Receiving and printing latest state message ...'
statemsg_ = receive(sub,10); %timeout after 10 seconds
% display dobot's states
disp (statemsg_.Pos.X)
disp (statemsg_.Pos.Y)
disp (statemsg_.Pos.Z)
disp (statemsg_.JointAngles)
disp (statemsg_.Pump)


%% go home
homesvc_ = rossvcclient('/dobot_magician/home');
homemsg_ = rosmessage(homesvc_);
% set home cartesian postions
homemsg_.Pos.X = 0.2;
homemsg_.Pos.Y = 0.0;
homemsg_.Pos.Z = 0.0;
homesvc_.call(homemsg_);% send message

%% move end effector to Cartesian co-ordinate
cartsvc_ = rossvcclient('/dobot_magician/cart_pos');

%create and populate service request (all in metres)
cartmsg_ = rosmessage(cartsvc_);
cartmsg_.Pos.X = 0.2;
cartmsg_.Pos.Y = 0.1;
cartmsg_.Pos.Z = -0.02;
cartsvc_.call(cartmsg_);% send message


%% set joint angles
jangsvc_ = rossvcclient('/dobot_magician/joint_angs');
% create and populate service request (all in radians)
jangmsg_ = rosmessage(jangsvc_);
jangmsg_.JointAngles = [ -0.2, 0.8, 0.6, -0.4 ];
jangsvc_.call(jangmsg_);

%% set pump compressor
airsvc_ = rossvcclient('/dobot_magician/air');
airmsg_ = rosmessage(airsvc_);
airmsg_.CtrlEnable=1;% turn pump on
airmsg_.Gripped=0;% 0 for compressor
airsvc_.call(airmsg_);% send message

%% set pump suction
airsvc_ = rossvcclient('/dobot_magician/air');
airmsg_ = rosmessage(airsvc_);
airmsg_.CtrlEnable=1;% turn pump on
airmsg_.Gripped=1;% 1 for suction
airsvc_.call(airmsg_);% send message

%% turn pump off
airmsg_.CtrlEnable=0;% turn pump off
airsvc_.call(airmsg_);% send message

%% stepper
steppersvc_ = rossvcclient('/dobot_magician/stepper');
steppermsg_ = rosmessage(steppersvc_);
steppermsg_.StepperSel=0;% choose which peripheral output stepper is present at ie (stepper1 or stepper2 port)
steppermsg_.Speed=5000;% choose speed (pulses/sec) of stepper motor -ve values is clockwise, +ve values is counter clockwise 
steppersvc_.call(steppermsg_);% send message

%% turn stepper off
steppermsg_.Speed=0;% choose speed (pulses/sec) of stepper motor -ve values is clockwise, +ve values is counter clockwise 
steppersvc_.call(steppermsg_);% send message

%% conveyor
conveyorsvc_ = rossvcclient('/dobot_magician/conveyor');
conveyormsg_ = rosmessage(conveyorsvc_);
conveyormsg_.StepperSel=0;% choose which peripheral output stepper is present at ie (0 is for stepper1 or 1 is for stepper2 port)
conveyormsg_.Speed=-5000;% choose speed (pulses/sec) of conveyor belt stepper motor, -ve values is right and +ve values is left, min speed is -20000 pulses/sec and max speed is 20000 pulses/sec   
conveyormsg_.Distance=100;% distance of travel in mm
conveyorsvc_.call(conveyormsg_);% send message

%% linear rail
linear_rail_svc_ = rossvcclient('/dobot_magician/linear_rail');
linear_rail_msg_ = rosmessage(linear_rail_svc_);
linear_rail_msg_.StepperSel=0;% choose which peripheral output stepper is present at ie (0 is for stepper1 or 1 is for stepper2 port)
linear_rail_msg_.Speed=3000;% choose speed (pulses/sec) of conveyor belt stepper motor, -ve values is right and +ve values is left, min speed is -4000 pulses/sec and max speed is 4000 pulses/sec
linear_rail_msg_.Distance=100;% distance of travel in mm
linear_rail_svc_.call(linear_rail_msg_);% send message

