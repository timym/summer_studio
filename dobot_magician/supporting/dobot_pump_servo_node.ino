#include <ros.h>
#include <std_msgs/Float64.h>
#include <Servo.h>

#define SERVO_PIN 9
#define BAUD 115200
#define SET_TOPIC "/dobot_magician/pump_servo/set"
#define GET_TOPIC "/dobot_magician/pump_servo/get"
#define RAD2DEG 57.296

ros::NodeHandle nh;
std_msgs::Float64 servo_msg_;
ros::Publisher spub(GET_TOPIC, &servo_msg_);
Servo servo_; int curr_pos_deg_;

void set_servo_rad(double rad)
{
  int p_ = (rad * RAD2DEG) + 90;
  if((p_>=0 && p_<=180) && p_!=curr_pos_deg_)
    { servo_.write(p_); curr_pos_deg_ = p_; }
}

void servo_cb(const std_msgs::Float64& msg)
{
  set_servo_rad(msg.data);
}
ros::Subscriber<std_msgs::Float64> ssub(SET_TOPIC, servo_cb);

void get_servo_rad()
{
  curr_pos_deg_ = servo_.read();
  servo_msg_.data = (curr_pos_deg_ - 90) / RAD2DEG;
}

void setup()
{
  // node related
  nh.getHardware()->setBaud(BAUD);
  nh.initNode();

  // servo related
  servo_.attach(SERVO_PIN); delay(10);
  get_servo_rad(); set_servo_rad(0.0);
  nh.subscribe(ssub); nh.advertise(spub);
}

void loop()
{
  delay(20); nh.spinOnce();
  get_servo_rad(); spub.publish(&servo_msg_);
}
