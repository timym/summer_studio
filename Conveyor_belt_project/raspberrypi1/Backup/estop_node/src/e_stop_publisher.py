#!/usr/bin/env python

from std_msgs.msg import Bool
import rospy
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(25, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def pub_estop():
    ir_pub = rospy.Publisher('/e_stop/state', Bool, queue_size=1)
    rospy.init_node('e_stop', anonymous=True)
    while not rospy.is_shutdown():
             if GPIO.input(25)==1:
                 ir_pub.publish(True)
             else:
                 ir_pub.publish(False)

if __name__ == '__main__':
    try:
        pub_estop()
    except rospy.ROSInterruptException:
        pass
