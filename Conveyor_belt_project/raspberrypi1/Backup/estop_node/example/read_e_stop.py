
#!/usr/bin/env python
#this code creates a client to send lights

import numpy
import rospy
from std_msgs.msg import Bool

def callback(data):
    print data
def listener():

    rospy.init_node("e_stop")
    rospy.Subscriber("e_stop/state", Bool, callback)

   # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
if __name__ == '__main__':
    listener()
