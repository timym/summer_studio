#!/usr/bin/env python

from std_msgs.msg import Bool
import rospy

def pub_go_pickup():
    go_pickup_pub = rospy.Publisher('/go_pickup/state', Bool, queue_size=1)
    rospy.init_node('go_pickup', anonymous=True)
    while not rospy.is_shutdown():

                 go_pickup_pub.publish(True)


if __name__ == '__main__':
    try:
        pub_go_pickup()
    except rospy.ROSInterruptException:
        pass
