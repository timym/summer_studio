#!/usr/bin/env python
import RPi.GPIO as GPIO
import rospy
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(18,GPIO.OUT)
GPIO.setup(23,GPIO.OUT)
GPIO.setup(24,GPIO.OUT)
from stack_light.srv import SetLights
from stack_light.msg import *

class Stack_Light_node:
  def __init__(self): # 'main'
      self.Red=1
      self.Amber=1
      self.Green=1
      self.lights(self.Red,self.Amber,self.Green)
      rospy.init_node('stack_lights_node', anonymous=True)

      # ROS services
      self._jang_srv = rospy.Service('/lights', SetLights, self.lights_svc)

      rospy.spin();


  def lights(self,Red_,Amber_,Green_): 
      GPIO.output(18,Red_)
      GPIO.output(23,Amber_)
      GPIO.output(24,Green_)
      self.Red=Red_
      self.Amber=Amber_
      self.Green=Green_


  def setLights(self, lights): # ros input to control lights
 

      red_ros=int(lights[0])
      amber_ros=int(lights[1])
      green_ros=int(lights[2])
      self.lights(red_ros,amber_ros,green_ros)

  

  def lights_svc(self, req): # set light service
    self.setLights(req.lights); return []

if __name__ == '__main__':
	try:
	  	sl_node_ = Stack_Light_node()
	finally:
		sl_node_.lights(0,0,0)