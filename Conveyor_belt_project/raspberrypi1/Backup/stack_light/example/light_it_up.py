
#!/usr/bin/env python
#this code creates a client to send lights

import numpy
import rospy
from stack_light.srv import SetLights
from stack_light.msg import *

    # def get_state(self):
    #     rospy.Subscriber('/dobot_magician/state',State,self.callback)


def send_lights(l1, l2, l3):
    rospy.wait_for_service('/lights')
    try:
        light_service = rospy.ServiceProxy('/lights', SetLights) # subscribe to /lights

        lights = [l1, l2, l3] # set joint angles in rad
        print (lights)
        msg = light_service(lights) # send lights


    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

if __name__ == "__main__":

     send_lights(False,False,True);
