
#!/usr/bin/env python
#this code creates a client to send joint angles,cartesian points and stepper values to dobot via ROS


import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(4,GPIO.IN)
GPIO.setup(18, GPIO.IN)

import numpy
import rospy
from dobot_magician.srv import *
from dobot_magician.srv import Pickup
from dobot_magician.msg import *
from geometry_msgs.msg import Point
from std_msgs.msg import Bool
from time import sleep
from stack_light.srv import Setlights
from stack_light.msg import *

class Dobot_Subscriber_and_Client:


    def init(self):

        self.current_x = None
        self.current_y = None
        self.current_z = None
        self.current_j1 = None
        self.current_j2 =None
        self.current_j3 = None
        self.current_j4 = None
        self.estop_status=None
	self.command_pickup=False




    def callback(self,the_msg):
        self.current_x = the_msg.pos.x
        self.current_y = the_msg.pos.y
        self.current_z = the_msg.pos.z
        self.current_j1 = the_msg.joint_angles[0]
        self.current_j2 = the_msg.joint_angles[1]
        self.current_j3 = the_msg.joint_angles[2]
        self.current_j4 = the_msg.joint_angles[3]
	

    def estop_callback(self,estop_data):       
        self.estop_status=estop_data.data

    def handle_go_pickup(self,req):
	if req.pickup_sel==True:
		print "command pickup"
		self.command_pickup=True	
  
    def send_lights(self,l1, l2, l3):
	rospy.wait_for_service('/lights')
	try:
		light_service = rospy.ServiceProxy('/lights', Setlights) # subscribe to /lights
	
		lights = [l1, l2, l3] # setlights
		msg = light_service(lights) # send lights


    	except rospy.ServiceException, e:
        	print "Service call failed: %s"%e

    def update_callback(self):

        rospy.Subscriber('/dobot_magician/state',State,self.callback)
        rospy.Subscriber('e_stop/state', Bool,self.estop_callback)
        rospy.Service('/go_pickup/state', Pickup , self.handle_go_pickup)
        light_service = rospy.ServiceProxy('/lights', Setlights) # subscribe to /lights
       


    def send_joint_angles(self,j1, j2, j3, j4):
        rospy.wait_for_service('/dobot_magician/joint_angs')
        try:
            joint_angle_service = rospy.ServiceProxy('/dobot_magician/joint_angs', SetPosAng) # subscribe to /dobot_magician/joint_angs service
            JointAngles = [j1, j2, j3, j4] # set joint angles in rad
            msg = joint_angle_service(JointAngles) # send joint angles


        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def send_cartesian_position(self,x,y,z):
        rospy.wait_for_service('/dobot_magician/cart_pos')
        try:
            cartesian_service = rospy.ServiceProxy('/dobot_magician/cart_pos', SetPosCart) # subscribe to /dobot_magician/joint_angs service

            cart=Point(x,y,z)
            msg = cartesian_service(cart) # send joint angles


        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def send_stepper(self,stepper_select,speed):# 0-stepper 1 & 1-stepper 2, -20000 to 20000
        rospy.wait_for_service('/dobot_magician/stepper')
        try:
            stepper_service = rospy.ServiceProxy('/dobot_magician/stepper', SetStepper) # subscribe to /dobot_magician/stepper service

            msg = stepper_service(stepper_select,speed) # send stepper


        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    def send_pump(self,ctrlEnable,Gripped): #1-0n & 0-off, 0-ungripped,1-gripped
        rospy.wait_for_service('dobot_magician/pump')
        try:
            pump_service = rospy.ServiceProxy('dobot_magician/pump', SetPump) # subscribe to /dobot_magician/pump service

            msg = pump_service(ctrlEnable,Gripped) # send pump


        except rospy.ServiceException, e:
            print "Service call failed: %s"%e


if __name__ == "__main__":
     rospy.init_node("picknplace")
     DSC_=Dobot_Subscriber_and_Client() #create object
     DSC_.init() #initialise
     DSC_.update_callback()

     try:
         while not rospy.core.is_shutdown():

		
	        if DSC_.command_pickup==True:
			  DSC_.send_lights(0,1, 0)
			  print(1)
		          DSC_.send_pump(0,0) # ungrip
		          print(2)
		          DSC_.send_cartesian_position(0.2,0,0.08)# get into starting position
		          print(3)
		          DSC_.send_joint_angles(DSC_.current_j1,DSC_.current_j2,DSC_.current_j3,0); # home gripper
		          print(4)
		          DSC_.send_pump(1,0) # ungrip
		          print(5)
		          while GPIO.input(4)==1 and DSC_.estop_status==True:
		          	 DSC_.send_stepper(0,-5000) # move conveyor right to dobot  stop when sensor detects part
		          DSC_.send_stepper(0,-5000) # keep moving conveyor right to dobot for 1 second
		          sleep(0.5)
		          DSC_.send_stepper(0,0) 
		          print(10)
		          DSC_.send_cartesian_position(DSC_.current_x,DSC_.current_y,0.035) #move gripper down
		          print(11)
		          sleep(1)
		          print(13)
		          DSC_.send_pump(1,1) # grip
		          print(14)
		          sleep(1)
		          print(16)
		          DSC_.send_joint_angles(DSC_.current_j1,DSC_.current_j2,DSC_.current_j3,1.57); # twist part to remove
		          print(17)
		          sleep(3)
		          print(18)
		          DSC_.send_joint_angles(DSC_.current_j1,DSC_.current_j2,DSC_.current_j3,0); # twist part to remove
		          print(19)
		          sleep(3)
		          print(20)
		          DSC_.send_cartesian_position(DSC_.current_x,DSC_.current_y,0.1) #move gripper up
		          print(21)
		          sleep(1)
		          print(22)
		          DSC_.send_joint_angles(1.57,DSC_.current_j2,DSC_.current_j3,DSC_.current_j4); # turn
		          print(23)
		          sleep(1)
		          print(24)
		          DSC_.send_pump(1,0) # ungrip to release part
		          print(25)
		          sleep(1)
		          print(26)
		          DSC_.send_joint_angles(0,DSC_.current_j2,DSC_.current_j3,DSC_.current_j4); # turn
			  while GPIO.input(18)==0:
		                   DSC_.send_stepper(0,5000) # return convyor belt		  
		          print(29)
		          DSC_.send_stepper(0,0)# stop conveyor
		          print(30)
          		  DSC_.send_pump(0,0) #turn pump off
			  DSC_.command_pickup=False
			  DSC_.send_lights(0,0, 1)


     except KeyboardInterrupt:
         logdebug("keyboard interrupt, shutting down")
     rospy.core.signal_shutdown('keyboard interrupt')




     # DSC_.send_stepper(0,0)# stop conveyor
     # DSC_.send_pump(0,0) #turn pump off
     # rospy.spin()
