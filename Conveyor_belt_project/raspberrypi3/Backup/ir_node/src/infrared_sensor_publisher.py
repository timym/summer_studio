#!/usr/bin/env python

from std_msgs.msg import Bool
import rospy
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(4,GPIO.IN)



def pub_ir():
    ir_pub = rospy.Publisher('/ir/state', Bool, queue_size=1)
    rospy.init_node('ir', anonymous=True)
    while not rospy.is_shutdown():
             if GPIO.input(4)==1:
                 ir_pub.publish(True)
                 print "clear"
             else:
                 ir_pub.publish(False)
                 print "obstructed"


if __name__ == '__main__':
    try:
        pub_ir()
    except rospy.ROSInterruptException:
        pass
