#!/bin/bash

source /opt/ros/kinetic/setup.bash
source /home/ubuntu/catkin_ws/devel/setup.bash
export ROS_MASTER_URI=http://192.168.1.201:11311
export ROS_HOSTNAME=192.168.1.203


xterm -hold -e  'sleep 20 ; cd /home/ubuntu/Desktop/summer_studio/Conveyor_belt_project/raspberrypi3 ; git pull'&
xterm -hold -e  'sleep 20 ; roslaunch dobot_magician dobot_magician.launch'&
#xterm -hold -e  'sleep 20 ; cd /home/ubuntu/Desktop/summer_studio/Conveyor_belt_project/raspberrypi3 ; python Pick_and_place.py'&
xterm -hold -e  'sleep 20 ; roslaunch /home/ubuntu/catkin_ws/src/video_stream_opencv-master/launch/camera.launch'

